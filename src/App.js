
import React, { useState } from 'react';
import './App.css';

function App() {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    file: null,
  });

  const [currentStep, setCurrentStep] = useState(1);

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleFileChange = (e) => {
    setFormData({
      ...formData,
      file: e.target.files[0],
    });
  };

  const handleNext = () => {
    setCurrentStep((prevStep) => prevStep + 1);
  };

  const handlePrev = () => {
    setCurrentStep((prevStep) => prevStep - 1);
  };



  const handleSubmit = async () => {
    console.log('Uploading file...');
    await new Promise((resolve) => setTimeout(resolve, 2000));
    console.log('File uploaded successfully!');
  };


  return (
    <div className="App">
      <h1> Form</h1>
      <form>
        {currentStep === 1 && (
          <div>
            <h3> Personal Information</h3>
            <label htmlFor="name">Name:</label>
            <input type="text" id="name" name="name" value={formData.name} onChange={handleChange} required />

            <label htmlFor="email">Email:</label>
            <input type="email" id="email" name="email" value={formData.email} onChange={handleChange} required />
          </div>
        )}

        {currentStep === 2 && (
          <div>
            <h3> Upload File</h3>
            <input type="file" id="file" name="file" onChange={handleFileChange} accept=".pdf, .doc, .docx" />
          </div>
        )}

        {currentStep === 3 && (
          <div>
            <h3>Step 3: Confirmation</h3>
            <p>Name: {formData.name}</p>
            <p>Email: {formData.email}</p>
            <p>File: {formData.file ? formData.file.name : 'No file selected'}</p>
          </div>
        )}

        { }
        <div>
          {currentStep > 1 && <button type="button" onClick={handlePrev}>Previous</button>}
          {currentStep < 3 ? <button type="button" onClick={handleNext}>Next</button> : <button type="button" onClick={handleSubmit}>Submit</button>}
        </div>
      </form>
    </div>
  );
}

export default App;
